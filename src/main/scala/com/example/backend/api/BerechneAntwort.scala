/*
 * Copyright (c) 2021 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.example.backend.api

import cats.effect._
import cats.syntax.all._
import com.example.backend.models._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import fs2.Stream
import kantan.csv._
import kantan.csv.ops._
import kantan.csv.generic._
import kantan.csv.java8._
import kantan.csv.refined._
import org.http4s._
import org.http4s.dsl._
import org.slf4j.LoggerFactory
import sttp.capabilities.WebSockets
import sttp.capabilities.fs2.Fs2Streams
import sttp.client3._
import sttp.client3.SttpBackend
import sttp.model._
import sttp.tapir._
import sttp.tapir.codec.refined._
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._
import sttp.tapir.server.http4s._
import fs2.text

final class BerechneAntwort[F[_]: Concurrent: ContextShift: Timer](
    backend: SttpBackend[F, Fs2Streams[F] with WebSockets]
) extends Http4sDsl[F] {
  private val log = LoggerFactory.getLogger(getClass())

  private val berechneAntwort: HttpRoutes[F] = Http4sServerInterpreter[F]().toRoutes(BerechneAntwort.berechneAntwort) {
    qps =>
      for {
        urls <- Sync[F].delay(BerechneAntwort.dateiUrisAusQueryParams(qps))
        _    <- Sync[F].delay(log.info(s"Endpunkt angefragt mit URLs: ${urls.mkString(", ")}"))
        uris <- Sync[F].delay(urls.flatMap(u => sttp.model.Uri.parse(u).toOption))
        _    <- Sync[F].delay(log.info(s"Parsed URIs: ${uris.mkString(", ")}"))
        bytes = Stream.evalSeq(uris.traverse { uri =>
          val req = basicRequest.get(uri).response(asByteArrayAlways)
          req.send(backend)
        })
        files = bytes.flatMap(resp => Stream.emits(resp.body).through(text.utf8Decode))
        data = files.flatMap(f =>
          Stream.emits(
            f.asCsvReader[Statistik](rfc.withHeader)
              .map {
                case Left(error) =>
                  log.error(s"CSV-Parser-Fehler: $error")
                  None
                case Right(row) => row.some
              }
              .toList
              .flatten
          )
        )
        stats        <- data.compile.toList
        mostSpeeches <- Sync[F].delay(Antwort.berechneHaeufigste(stats)(_.datum.getYear() === 2013))
        mostSecurity <- Sync[F].delay(Antwort.berechneHaeufigste(stats)(_.thema === "Innere Sicherheit"))
        leastWordy   <- Sync[F].delay(Antwort.berechneWenigsteWorte(stats))
      } yield Antwort(mostSpeeches = mostSpeeches, mostSecurity = mostSecurity, leastWordy = leastWordy).asRight
  }

  val routes: HttpRoutes[F] = berechneAntwort
}

object BerechneAntwort {
  val example = Antwort(
    mostSpeeches = Option("Franz Kafka"),
    mostSecurity = Option("Albert Camus"),
    leastWordy = Option("Yoko Ogawa")
  )

  val dateiUrisAusQueryParams: QueryParams => List[DateiUri] = qs =>
    qs.toMap.view.filterKeys(_.matches("^url\\d+$")).values.flatMap(s => DateiUri.from(s).toOption).toList

  //val extrahiereParameter: EndpointInput[List[DateiUri]] =
  //  queryParams().map(ps => dateiUrisAusQueryParams)(ls =>
  //    QueryParams.fromSeq((1 to ls.length).map(n => s"url$n").zip(ls))
  //  )

  val berechneAntwort: Endpoint[QueryParams, StatusCode, Antwort, Any] =
    endpoint.get
      .in("berechne")
      .in(queryParams)
      .errorOut(statusCode)
      .out(jsonBody[Antwort].example(example))
      .description("Lädt die angegebenen Dateien herunter und berechnet die spezifizierte Antwort.")
}
