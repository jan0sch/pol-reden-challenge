/*
 * Copyright (c) 2021 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.example.backend.models

import java.time.LocalDate

/**
  * Basismodell für eine Zeile in der Statistik (CSV-Datei).
  *
  * @param redner  Der Name der Politikers.
  * @param thema   Das Thema der Rede.
  * @param datum   Das Datum der Rede.
  * @param woerter Die Anzahl der gesprochenen Wörter.
  */
final case class Statistik(redner: Redner, thema: Thema, datum: LocalDate, woerter: Wortanzahl)
