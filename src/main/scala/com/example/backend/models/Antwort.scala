/*
 * Copyright (c) 2021 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.example.backend.models

import cats.effect._
import cats.kernel.Order
import cats.syntax.all._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import fs2.Stream
import io.circe._
import io.circe.generic.semiauto._
import io.circe.refined._

/**
  * Antwortmodell für die Response des Service.
  *
  * @param mostSpeeches Welcher Politiker hielt im Jahr 2013 die meisten Reden?
  * @param mostSecurity Welcher Politiker hielt die meisten Reden zum Thema ”Innere Sicherheit”?
  * @param leastWordy   Welcher Politiker sprach insgesamt die wenigsten Wörter?
  */
final case class Antwort(mostSpeeches: Option[Redner], mostSecurity: Option[Redner], leastWordy: Option[Redner])

object Antwort {

  implicit val decodeCirce: Decoder[Antwort] = deriveDecoder[Antwort]
  implicit val encodeCirce: Encoder[Antwort] = deriveEncoder[Antwort]

  /**
    * Berechnet die häufigste Redner:in unter Nutzung der übergebenen
    * Relevanzfunktion.
    *
    * Beispiele für Relevanzfunktionen:
    * 1. `_.thema === "Innere Sicherheit"`
    * 2. `_.datum.getYear() === 2013`
    *
    * Insofern das Ergebnis nicht eineindeutig ist (z.B. zwei Redner:innen
    * mit gleicher Häufigkeit), wird eine leere Option (`None`) zurückgegeben.
    *
    * @param stats Eine Liste mit Statistiken zu Reden.
    * @param fn    Die entsprechende Relevanzfunktion.
    * @return Eine Option zu einem Namen, insofern das Ergebnis eineindeutig ist.
    */
  def berechneHaeufigste(stats: List[Statistik])(fn: Statistik => Boolean): Option[Redner] = {
    val meiste = stats
      .foldLeft(Map.empty[Redner, Int]) { (acc, st) =>
        if (fn(st))
          acc + (st.redner -> (acc.getOrElse(st.redner, 0) + 1))
        else
          acc
      }
      .toList
      .sortBy(_._2)
      .reverse
    meiste.headOption.flatMap { h =>
      val (redner, anzahl) = h
      if (meiste.drop(1).headOption.map(_._2 === anzahl).getOrElse(false))
        None
      else
        redner.some
    }
  }

  /**
    * Berechnet die Redner:in mit der geringsten Wortanzahl.
    *
    * Insofern das Ergebnis nicht eineindeutig ist (z.B. zwei Redner:innen
    * mit gleicher Häufigkeit), wird eine leere Option (`None`) zurückgegeben.
    *
    * @param stats Eine Liste mit Statistiken zu Reden.
    * @return Eine Option zu einem Namen, insofern das Ergebnis eineindeutig ist.
    */
  def berechneWenigsteWorte(stats: List[Statistik]): Option[Redner] = {
    implicit val ordering: Ordering[Wortanzahl] = Order[Wortanzahl].toOrdering

    val wenigste = stats
      .foldLeft(Map.empty[Redner, Wortanzahl]) { (acc, st) =>
        if (acc.getOrElse(st.redner, Wortanzahl.MaxValue) > st.woerter)
          acc + (st.redner -> st.woerter)
        else
          acc
      }
      .toList
      .sortBy(_._2)
    wenigste.headOption.flatMap { h =>
      val (redner, anzahl) = h
      if (wenigste.drop(1).headOption.map(_._2 === anzahl).getOrElse(false))
        None
      else
        redner.some
    }
  }

}
