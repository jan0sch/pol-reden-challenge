/*
 * Copyright (c) 2021 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.example.backend

import eu.timepit.refined.api._
import eu.timepit.refined.auto._
import eu.timepit.refined.boolean._
import eu.timepit.refined.cats._
import eu.timepit.refined.collection._
import eu.timepit.refined.numeric._
import eu.timepit.refined.string._

package object models {

  type DateiUri = String Refined Uri
  object DateiUri extends RefinedTypeOps[DateiUri, String] with CatsRefinedTypeOpsSyntax

  type Redner = String Refined NonEmpty
  object Redner extends RefinedTypeOps[Redner, String] with CatsRefinedTypeOpsSyntax

  type Thema = String Refined (Trimmed And NonEmpty)
  object Thema extends RefinedTypeOps[Thema, String] with CatsRefinedTypeOpsSyntax

  type Wortanzahl = Int Refined NonNegative
  object Wortanzahl extends RefinedTypeOps[Wortanzahl, Int] with CatsRefinedTypeOpsSyntax {
    val MaxValue: Wortanzahl = 2147483647
    val MinValue: Wortanzahl = 0
  }

}
